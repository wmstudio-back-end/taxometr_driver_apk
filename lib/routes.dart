import 'package:need_car/widgets/pages/auth/auth_carousel.dart';
import 'package:need_car/widgets/pages/main_page.dart';

var routes = {
  '/': (_) => AuthCarousel(),
  '/main': (_) => MainPage(),
// '/offers':        () => OffersPage(),
// '/history':       () => HistoryPage(),
// '/info':          () => InfoPage(),
// '/notifications': () => NotificationsPage(),
// '/photocontrol':  () => PhotoControl(),
};