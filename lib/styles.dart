import 'package:flutter/material.dart';

var themeData = new ThemeData(
  scaffoldBackgroundColor: Colors.white,
  accentColor: Color(0xFFFFDB4D),
  appBarTheme: AppBarTheme(
    iconTheme: IconThemeData(
      color: Colors.black,
    ),
    color: Color(0xFF4F4E5D),
    elevation: 0,
  ),
  iconTheme: IconThemeData(
    color: Color(0xFFFFDB4D),
  ),
  textTheme: TextTheme(
    headline: TextStyle(
      fontFamily: 'Open Sans',
      fontSize: 14,
      fontWeight: FontWeight.bold,
    ),
    title: TextStyle(
      color: Colors.black,
      fontFamily: 'Open Sans',
      fontWeight: FontWeight.bold,
      fontSize: 16,
    ),
    body1: TextStyle(
      fontFamily: 'Open Sans',
      fontSize: 10,
    ),
    body2: TextStyle(
      fontFamily: 'Open Sans',
      fontSize: 12,
    ),
    button: TextStyle(
      fontFamily: 'Open Sans',
      fontSize: 12,
      fontWeight: FontWeight.bold,
      color: Color(0xFF4F4E5D),
    ),
  ),
);