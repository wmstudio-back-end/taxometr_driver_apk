import 'package:flutter/material.dart';
import 'package:need_car/routes.dart';
import 'package:need_car/styles.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Need Car',
      theme: themeData,
      initialRoute: '/',
      routes: routes
    );
  }
}
