import 'package:need_car/store/actions/offers.dart';
import 'package:need_car/store/state.dart';

AppState appStateReducer(AppState state, action) {
  return state;
}

List<Offer> offersReducer(AppState state, action) {
  if (action is AddOffer) {
    return List.unmodifiable(<Offer> [
      ...state.offers,
      action.offer,
    ]);
  }
  if (action is RemoveOffer) {
    List<Offer> newOfferList = state.offers.where((offer) => offer.id == action.id);
    return List.unmodifiable(<Offer> [
      ...newOfferList,
    ]);
  }
}

