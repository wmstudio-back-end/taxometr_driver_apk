class AppState {
  final List<Offer> offers;
  final User user;

  AppState({
    this.offers,
    this.user,
  });

  AppState.initialStae() : 
    offers = List.unmodifiable(<Offer>[]),
    user = new User();
}

class Offer {
  String id;
  String from;
  String to;
  String createdAt;

  Offer({
    this.id,
    this.from,
    this.to,
    this.createdAt,
  });
}

class User {
  String id;
  String fullName;
}