import 'package:need_car/store/state.dart';

class AddOffer {
  Offer offer;
  AddOffer(this.offer);
}

class RemoveOffer {
  String id;
  RemoveOffer(this.id);
}