import 'package:flutter/material.dart';
import 'package:need_car/widgets/buttons/button_primary.dart';
import 'package:need_car/widgets/forms/checkbox_circle.dart';
import 'package:need_car/widgets/forms/form_text_field.dart';
import 'package:need_car/widgets/helpers/empty.dart';
import 'package:need_car/widgets/layout/layout.dart';

class SelectPage extends StatefulWidget {

  final String title;
  final List<String> items;
  final bool singleItemMode;
  final bool withSearchBar;
  final String searchBarLabel;
  final Function onAccept;
  final bool isRequired;

  SelectPage({
    @required this.title,
    @required this.items,
    @required this.onAccept,
    this.isRequired = false,
    this.singleItemMode = true,
    this.withSearchBar = false,
    this.searchBarLabel,
  }) {
    if (withSearchBar && searchBarLabel == null) {
      throw new Exception('Searchbar need a label!');
    }
  }

  @override
  _SelectPageState createState() => _SelectPageState();
}

class _SelectPageState extends State<SelectPage> {

  List<int> _selected;
  final _searchQuery = TextEditingController();

  bool get _valid {
    if (_selected.length > 0) {
      return true;
    }
    return false;
  }

  @override
  void initState() {
    _clear();
    super.initState();
  }

  void _clear() {
    _selected = <int>[];
  }

  void _select(bool state, int index) {
    if (state) {
      if (widget.singleItemMode) {
        _clear();
      }
      _selected.add(index);
    } else {
      if (_selected.contains(index)) {
        if (!widget.singleItemMode) {
          _selected.removeWhere((item) => item == index);
        }
      }
    }
    setState(() {});
  }

  Widget _buildSearchBar(BuildContext context) {
    if (widget.withSearchBar) {
      return NCTextField(
        label: widget.searchBarLabel,
        controller: _searchQuery,
      );
    }
    return Empty();
  }

  List<String> _collectResult() {
    List<String> result = [];
    _selected.forEach((item) {
      result.add(widget.items[item]);
    });
    return result;
  }

  Widget _buildList(BuildContext context) {
    return Column(
      children: <Widget>[
        ...List.generate(widget.items.length, (int index) {
          if (widget.items[index].toLowerCase().contains(
              _searchQuery.text.toLowerCase())) {
            String item = widget.items[index];
            UniqueKey key = new UniqueKey();
            return Container(
              key: key,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Color(0xFFF4F4F6),
                  ),
                ),
              ),
              child: CircleCheckbox(
                value: _selected.contains(index),
                label: Text(
                  item,
                  style: Theme.of(context).textTheme.headline.copyWith(
                    fontWeight: FontWeight.normal,
                  ),
                ),
                onChange: (bool state) {
                  _select(state, index);
                },
              ),
            );
          }
          return Empty();
        }).toList(),
      ],
    );
  }



    @override
  Widget build(BuildContext context) {
    UniqueKey key = new UniqueKey();
    return Layout(
      appBarBottomPanel: Builder(builder: _buildSearchBar),
      title: widget.title,
      child: SingleChildScrollView(child: Builder(builder: _buildList)),
      bottomPanel:
      PrimaryButton(
        key: key,
        enabled:  _valid ,
        text: 'Подтвердить',
        onPressed: () {
          List<String> result = _collectResult();
          widget.onAccept(result);
        },
      ),
    );
  }
}

