import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class CircleCheckbox extends StatefulWidget {

  final Widget    label;
  final bool      value;
  final Function  onChange;
  final UniqueKey key;
  final bool      reverse;

  CircleCheckbox({
    @required this.label,
    @required this.onChange,
    this.value = false,
    this.key,
    this.reverse = false,
  });

  @override
  _CircleCheckboxState createState() => _CircleCheckboxState();
}

class _CircleCheckboxState extends State<CircleCheckbox> {

  bool _value;

  @override
  void initState() {
    _value = widget.value;
    super.initState();
  }

  void _toggleState() {
    setState(() {
      _value = !_value;
    });
    widget.onChange(_value);
  }

  Widget _buildCheckbox(BuildContext context) {

    List<Widget> _items = [
      Flexible(
        child: Column(
          children: [
            widget.label,
          ],
        ),
      ),
      SizedBox(
        width: 15,
      ),
      Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
          border: Border.all(
            color: Color(0xFFFFDB4D),
          ),
        ),
        child: Align(
          alignment: Alignment.center,
          child: Container(
            height: 12,
            width: 12,
            decoration: BoxDecoration(
              color: _value 
                ? Color(0xFFFFDB4D)
                : Colors.transparent,
            ),
          ),
        ),
      ),
    ];

    if (widget.reverse) {
      _items = _items.reversed.toList();
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: _items,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _toggleState,
      behavior: HitTestBehavior.opaque,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 15,
        ),
        child: Builder(
          builder: _buildCheckbox,
        ),
      ),
    );
  }
}