import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class NCTextField extends StatelessWidget {

  final String label;
  final Function onChange;
  final Function validator;
  final bool autoValidate;

  final TextEditingController controller;
  final List<MaskTextInputFormatter> inputFormatters;
  final bool autocorrect;
  final TextInputType keyboardType;
  final InputDecoration decoration;


  NCTextField({
    this.label,
    this.onChange,
    this.validator,
    this.autoValidate,

    this.autocorrect,
    this.controller,
    this.decoration,
    this.inputFormatters,
    this.keyboardType,

  });

  Widget _buildInput(BuildContext context) {
    if (label != null) {
      return  TextFormField(
//        onChanged: (String value) {
//          if (onChange != null) {
//            onChange(value);
//          }
//        },

          cursorColor: Colors.black,
          controller: controller ,
          inputFormatters: inputFormatters ,
          autocorrect: autocorrect ?? false,
          keyboardType: keyboardType ,
          decoration: decoration ?? InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            labelStyle: Theme.of(context).textTheme.body2.copyWith(
              color: Color(0xFFDBDBDB),
            ),
            labelText: '$label',
          ),
          autovalidate: autoValidate ?? false,
        validator: validator,
        );
    }
    return TextFormField(
      cursorColor: Colors.black,
//      onChanged: (String value) {
//        if (onChange != null) {
//          onChange(value);
//        }
//      },
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 10,
        ),
        border: InputBorder.none,
      ),
        autovalidate: autoValidate ?? false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            color: Color(0xFFF4F4F6),
          ),
        ),
      ),
      child: Builder(builder: _buildInput,)
    );
  }
}