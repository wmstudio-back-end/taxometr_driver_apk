import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:need_car/icons/n_c_icons_icons.dart';
import 'package:need_car/widgets/helpers/empty.dart';

enum _LayoutType {
  transparent,
  light,
}

class Layout extends StatelessWidget {

  final _LayoutType type;
  final String title;
  final Widget child;
  final Widget bottomPanel;
  final Widget appBarBottomPanel;
  final bool withDrawer;
  final key;

  final List<Map> _menuItems = [
    {
      'icon': NCIcons.nc_history,
      'title': 'История заказов',
      'to': '/history',
    }, {
      'icon': NCIcons.nc_notification,
      'title': 'Уведомления',
      'to': '/notifications',
    }, {
      'icon': NCIcons.nc_camera,
      'title': 'Фотоконтроль',
      'to': '/photocontrol',
    }, {
      'icon': NCIcons.nc_info,
      'title': 'Информация',
      'to': '/info',
    },
  ];
  
  Layout({
    @required this.child,
    this.type = _LayoutType.light,
    this.withDrawer = false,
    this.title,
    this.bottomPanel,
    this.appBarBottomPanel,
    this.key,
  });

  Layout.transparent(
    this.child,
    this.withDrawer,
    this.title,
    this.bottomPanel,
    this.appBarBottomPanel,
    this.key,
  ) : type = _LayoutType.transparent;

  Widget _buildMenu(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Column(
            children: [
              StartChangeButton(),
              // MenuItem(
              //   icon: Icon(
              //     NCIcons.nc_exit,
              //     color: Colors.black,
              //   ),
              //   child: Text('Открыть смену'),
              // ),
            ],
          ),
        ),
        Container(
          height: 10,
          color: Color(0xffE5E5E5),
        ),
        Expanded(
          child: Container(
            color: Color(0xffE5E5E5),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  MenuItem(
                    icon: Icon(
                      NCIcons.nc_time,
                      color: Colors.black,
                    ),
                    child: Text('Свободный таксометр'),
                  ),
                  Divider(
                    height: 1,
                    color: Color(0xffE5E5E5),
                  ),
                  MenuItem(
                    icon: Icon(
                      NCIcons.nc_history,
                      color: Colors.black,
                    ),
                    child: Text('История заказов'),
                  ),
                  Divider(
                    height: 1,
                    color: Color(0xffE5E5E5),
                  ),
                  MenuItem(
                    icon: Icon(
                      NCIcons.nc_info,
                      color: Colors.black,
                    ),
                    child: Text('Информация'),
                  ),
                  Divider(
                    height: 1,
                    color: Color(0xffE5E5E5),
                  ),
                  MenuItem(
                    icon: Icon(
                      NCIcons.nc_notification,
                      color: Colors.black,
                    ),
                    child: Text('Уведомления'),
                  ),
                  Divider(
                    height: 1,
                    color: Color(0xffE5E5E5),
                  ),
                  MenuItem(
                    icon: Icon(
                      NCIcons.nc_alert,
                      color: Colors.black,
                    ),
                    child: Text('Тревога'),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          height: 10,
          color: Color(0xffE5E5E5),
        ),
        Container(
          child: Column(
            children: [
              MenuItem(
                icon: Icon(
                  NCIcons.nc_cash_box,
                  color: Colors.black,
                ),
                child: Text('Центральная касса'),
              ),
              Divider(
                height: 1,
                color: Color(0xffE5E5E5),
              ),
              MenuItem(
                icon: Icon(
                  NCIcons.nc_balance,
                  color: Colors.black,
                ),
                child: Row(
                  // mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Баланс',
                        ),
                        Text(
                          'За сегодня',
                        ),
                      ],
                    ),
                    // Expanded(child: Container(),),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '-2507.08руб',
                        ),
                        Text(
                          '+237.руб',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildContent(BuildContext context) {
    return child;
  }

  Widget _buildBottomPanel(BuildContext context) {
    if (bottomPanel != null) {
      return Container(
        padding: EdgeInsets.all(10),
        child: bottomPanel
      );
    }
    return null;
  }

  Widget _buildAppBarBottomPanel(BuildContext context) {
    if (appBarBottomPanel != null) {
      return appBarBottomPanel;
    }
    return Empty();
  }

  Widget _buildDrawer(BuildContext context) {
    if (withDrawer) {
      return Drawer(
        child: Container(
          decoration: BoxDecoration(
            border: Border.fromBorderSide(
              BorderSide(
                color: Color(0xffF4F4F6),
              ),
            )
          ),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: Row(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xffF4F4F6),
                      ),
                    ),
                    Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'Балдыков Чед Дмитриевич',
                            softWrap: true,
                            style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          '+7 (912) 123 45 78',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Divider(
                height: 1,
                color: Color(0xffF4F4F6),
              ),
              Expanded(child: _buildMenu(context)),
            ],
          ),
        ),
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {

    double appBarHeight = appBarBottomPanel != null
      ? 113
      : 60;

    return Scaffold(
      key: key,
      drawer: _buildDrawer(context),
      appBar: title != null
        ? PreferredSize(
          preferredSize: Size.fromHeight(appBarHeight),
          child: AppBar(
            backgroundColor: type == _LayoutType.light 
              ? Colors.white
              : Colors.transparent,
            centerTitle: true,
            title: Text(
              '$title',
              style: Theme.of(context).textTheme.title,
            ),
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(60),
              child: Builder(builder: _buildAppBarBottomPanel)
            ),
          ),
        )
      : PreferredSize(
          preferredSize: Size.fromHeight(0),
          child: Empty(),
        ),
      // bottomNavigationBar: Builder(builder: _buildBottomPanel),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: _buildBottomPanel(context),
      ),
      body: Builder(builder: _buildContent),
    );
  }
}

class MenuItem extends StatelessWidget {

  final Icon icon;
  final Widget child;
  final Function onPressed;
  final color;

  MenuItem({
    this.icon,
    this.child,
    this.onPressed,
    this.color = Colors.white,
  });

  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onPressed,
      child: Container(
        color: color,
        padding: EdgeInsets.symmetric(
          horizontal: 20, 
          vertical: 15
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            icon,
            SizedBox(
              width: 20,
            ),
            child,
          ],
        ),
      ),
    );
  }
}

class StartChangeButton extends StatefulWidget {

  @override
  _StartChangeButtonState createState() => _StartChangeButtonState();
}

class _StartChangeButtonState extends State<StartChangeButton> {

  bool isActive = false;

  @override
  iniState() {
    super.initState();
  }

  void _toggleChange() {
    setState(() {
      isActive = !isActive;
    });
  }

  @override
  Widget build(BuildContext context) {

    Color _iconColor = Colors.black;
    Color _color = Color(0xffFFDB4D);
    String _text = 'Закрыть смену';

    if (isActive) {
      _iconColor = Colors.red;
      _text = 'Открыть смену';
      _color = Colors.white;
    }

    return MenuItem(
    icon: Icon(
        NCIcons.nc_exit,
        color: _iconColor,
      ),
      color: _color,
      child: Text(_text),
      onPressed: _toggleChange,
    );
  }
}