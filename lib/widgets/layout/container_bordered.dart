import 'package:flutter/material.dart';

class BorderedContainer extends StatelessWidget {

  final Widget child;
  final Color borderColor;
  final double borderRadius;
  final EdgeInsetsGeometry padding;

  BorderedContainer({
    @required this.child,
    this.borderColor = const Color(0xFFFFDB4D),
    this.borderRadius = 0,
    this.padding = const EdgeInsets.all(0),
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      decoration: BoxDecoration(
        border: Border.fromBorderSide(
          BorderSide(color: borderColor),
        ),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      ),
      child: child,
    );
  }
}