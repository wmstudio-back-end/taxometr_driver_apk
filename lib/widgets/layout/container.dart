import 'package:flutter/material.dart';

class PaddingContainer extends StatelessWidget {

  final Widget child;

  PaddingContainer({
    @required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: child,
    );
  }
}