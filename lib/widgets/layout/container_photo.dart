import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:need_car/icons/n_c_icons_icons.dart';
import 'package:need_car/widgets/buttons/button_circle.dart';
import 'package:need_car/widgets/layout/container_bordered.dart';
import 'package:need_car/widgets/pages/common/pick_photo.dart';

class PhotoContainer extends StatefulWidget {
  
  final String title;
  final String description;

  PhotoContainer({
    @required this.title,
    this.description,
  });

  @override
  PhotoContainerState createState() => PhotoContainerState();

}

class PhotoContainerState extends State<PhotoContainer> {

  bool _isPictureTaken = false;
  String _picturePath;

  Future _pickImage() async {
    // final myInherited = InheritedPickPhoto.of(context);
    var file = await ImagePicker.pickImage(source: ImageSource.camera);
    if (file != null) {
      setState(() {
        _picturePath = file.path;
        _isPictureTaken = true;
        // myInherited.addTrue(_isPictureTaken);
      });
    }
  }

  void _deletePicture() {
    // final myInherited = InheritedPickPhoto.of(context);
    var file = File(_picturePath);
    file.deleteSync();
    setState(() {
      _isPictureTaken = false;
      _picturePath = null;
      // myInherited.addFalse(_isPictureTaken);
    });
  }

  bool validate() {
    return _isPictureTaken;
  }

  Widget _buildPage(BuildContext context) {
    if (!_isPictureTaken) {
      return GestureDetector(
        onTap: () async {
          await _pickImage();
          PhotoValidator.of(context).validate();
          PhotoValidator.of(context).state.forceRebuild();
        },
        child: Container(
          height: 150,
          child: BorderedContainer(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(NCIcons.nc_camera),
                Container(
                  margin: EdgeInsets.only(bottom: 10, top: 10),
                  child: Text(
                    '${widget.title}',
                    style: Theme.of(context).textTheme.body1.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                widget.description != null
                  ? Container(
                      child: Text(
                        '${widget.description}',
                        style: Theme.of(context).textTheme.body1.copyWith(
                          color: Color(0xFFDBDBDB),
                        ),
                      )
                    )
                  : null,
              ],
            ),
          ),
        ),
      );
    }
    return Container(
      height: 150,
      child: BorderedContainer(
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: double.infinity,
              child: FittedBox(
                alignment: Alignment.center,
                fit: BoxFit.fitWidth,
                child: Image.file(File(_picturePath))
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: CircleButton(
                color: Color(0xffFFDB4D),
                icon: Icon(
                  Icons.close,
                  size: 10,
                  color: Colors.white,
                ),
                onPressed: () {
                  _deletePicture();
                  PhotoValidator.of(context).validate();
                  PhotoValidator.of(context).state.forceRebuild();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('\nКонтейнер отправляется на регистраци...');
    PhotoValidator.of(context)?.register(this);
    return Builder(builder: _buildPage);
  }
}