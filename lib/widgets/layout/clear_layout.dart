import 'package:flutter/material.dart';

class ClearLayout extends StatelessWidget {
  
  final Widget child;

  ClearLayout({
    @required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: child,
      ),
    );
  }
}