import 'dart:async';

class Debounce {

  Timer _timer;
  Duration duration;

  Debounce({this.duration});

  void run(Function action) {
    if (_timer != null) {
      _timer.cancel();
    }
    _timer = new Timer(duration, action);
  }
}