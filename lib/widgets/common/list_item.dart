import 'package:flutter/material.dart';

class ListItem extends StatelessWidget {

  final String title;

  ListItem({
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 25),
            height: 10,
            width: 10,
            color: Color(0xFFFFDB4D),
          ),
          Text(
            '$title',
            style: Theme.of(context).textTheme.body2,
          ),
        ],
      ),
    );
  }
}