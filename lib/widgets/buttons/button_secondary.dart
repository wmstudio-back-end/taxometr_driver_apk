import 'package:flutter/material.dart';

class SecondaryButton extends StatelessWidget {

  final String text;
  final Function onPressed;

  SecondaryButton({
    @required this.text,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      behavior: HitTestBehavior.opaque,
      child: Container(
        color: Colors.transparent,
        height: 50,
        child: Align(
          alignment: Alignment.center,
          child: Text(
            '$text',
            style: Theme.of(context).textTheme.button,
          ),
        ),
      ),
    );
  }
}