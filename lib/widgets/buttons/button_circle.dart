import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {

  final Function onPressed;
  final Icon icon;
  final Color color;
  final Color borderColor;
  final double size;

  CircleButton({
    @required this.onPressed,
    @required this.icon,
    this.borderColor = const Color(0xffFFFFFF),
    this.color = const Color(0xffFFFFFF),
    this.size = 20,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          border: Border.all(
            color: borderColor,
          ),
          color: color,
          shape: BoxShape.circle,
        ),
        child: icon,
      ),
    );
  }
}