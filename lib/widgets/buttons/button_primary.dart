import 'package:flutter/material.dart';

class PrimaryButton extends StatefulWidget {

  final String text;
  final Function onPressed;
  final bool enabled;

  PrimaryButton({
    key,
    @required this.text,
    @required this.onPressed,
    this.enabled = true,
  }) : super(key: key);

  @override
  _PrimaryButtonState createState() => _PrimaryButtonState();
}

class _PrimaryButtonState extends State<PrimaryButton> {
  
  bool _isEnabled;

  @override
  void initState() {
    _isEnabled = widget.enabled;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
       if (_isEnabled) {
         widget.onPressed();
       }
      },
      behavior: HitTestBehavior.opaque,
      child: Container(
        color: _isEnabled
          ? Color(0xffFFDB4D)
          : Color(0xffF4F4F6),
        height: 50,
        child: Align(
          alignment: Alignment.center,
          child: Text(
            '${widget.text}',
            style: Theme.of(context).textTheme.button.copyWith(
              color: _isEnabled 
                ? Color(0xff232227)
                : Color(0xffDBDBDB),
            ),
          ),
        ),
      ),
    );
  }
}