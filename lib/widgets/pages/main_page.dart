import 'package:flutter/material.dart';
import 'package:need_car/icons/n_c_icons_icons.dart';
import 'package:need_car/widgets/buttons/button_circle.dart';
import 'package:need_car/widgets/layout/layout.dart';
import 'package:need_car/widgets/pages/offers/main.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  YandexMapController _controller;

  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();
    return Layout(
      key: _key,
      withDrawer: true,
      child: Stack(
        children: [
          Container(
            child: YandexMap(
              onMapCreated: (controller) {
                _controller = controller;
              },
            ),
          ),
          Positioned(
            top: 20,
            left: 20,
            child: CircleButton(
              icon: Icon(
                Icons.menu,
                color: Colors.black,
              ),
              onPressed: () {
                _key.currentState.openDrawer();
              },
              size: 42,
              color: Colors.transparent,
              borderColor: Colors.transparent,
            ),
          ),
          Positioned(
            top: 20,
            right: 20,
            child: CircleButton(
              borderColor: Color(0xffF4F4F6),
              icon: Icon(
                Icons.menu,
                color: Colors.black,
                size: 15,
              ),
              size: 42,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => OffersPage(),
                  ),
                );
              },
            ),
          ),
          Positioned(
            bottom: 20,
            left: 20,
            child: CircleButton(
              borderColor: Color(0xffF4F4F6),
              icon: Icon(
                NCIcons.nc_user_location,
                color: Colors.black,
                // size: 15,
              ),
              size: 42,
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}