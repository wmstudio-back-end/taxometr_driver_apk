import 'package:flutter/material.dart';
import 'package:need_car/widgets/buttons/button_primary.dart';
import 'package:need_car/widgets/buttons/button_secondary.dart';
import 'package:need_car/widgets/layout/clear_layout.dart';
import 'package:need_car/widgets/pages/auth/auth_required_documents.dart';

class LoginSentence extends StatelessWidget {
  
  // void _toLoginPage() {

  // }

  void _toRegisterPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => RequiredDocumentsPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ClearLayout(
      child: Padding(
        padding: const EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FractionallySizedBox(
              widthFactor: 0.5,
              child: PrimaryButton(
                text: 'Регистрация',
                onPressed: () {
                  _toRegisterPage(context);   
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            FractionallySizedBox(
              child: SecondaryButton(
                text: 'Вход',
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}