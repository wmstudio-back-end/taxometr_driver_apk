import 'package:flutter/material.dart';
import 'package:need_car/widgets/buttons/button_primary.dart';
import 'package:need_car/widgets/common/list_item.dart';
import 'package:need_car/widgets/layout/container.dart';
import 'package:need_car/widgets/layout/layout.dart';
import 'package:need_car/widgets/pages/auth/auth_register_form.dart';

class RequiredDocumentsPage extends StatelessWidget {

  final List<String> _requiredDocs = [
    'Паспорт',
    'Водительское удостоверение',
    'Свидетельство о регистрации ТС',
  ];

  Widget _buildList(BuildContext context) {
    return Column(
      children: _requiredDocs.map((document) {
        return ListItem(title: document);
      }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Layout(
      title: 'Заявление о присоединении',
      child: Column(
        children: <Widget>[
          PaddingContainer(
            child: Text(
              'Заполните обязательные поля регистрационной формы. Проверьте Правильность введенных данных.\n\nВ процессе регстрации необходимо будет сделать фотографии следующих документов:',
              style: Theme.of(context).textTheme.body2,
            ),
          ),
          Builder(
            builder: _buildList,
          ),
        ],
      ),
      bottomPanel: PrimaryButton(
        text: 'Продолжить',
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (_) => RegisterFormPage()), 
          );
        },
      ),
    );
  }
}