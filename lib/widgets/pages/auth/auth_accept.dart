import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:need_car/widgets/buttons/button_primary.dart';
import 'package:need_car/widgets/forms/checkbox_circle.dart';
import 'package:need_car/widgets/layout/layout.dart';

class AcceptPage extends StatefulWidget {
  @override
  _AcceptPageState createState() => _AcceptPageState();
}

class _AcceptPageState extends State<AcceptPage> {
  bool get _valid {
    bool valid = true;
    int length = _items.length;
    for (int i = 0; i < length; i++) {
      if (!_items[i]['checked']) {
        valid = false;
        break;
      }
    }
    return valid;
  }

  static void _showDocument(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 100,
          width: 100,
          color: Colors.white,
        );
      }
    );
  }

  List<Map> _items = [
    {
      'text': Text(
        'Достоверность предоставленных мню сведений и данных подтверждаю',
        style: TextStyle(
          fontSize: 14,
        ),
      ),
      'checked': false,
    }, {
      'text': Text(
        'Даюсогласие на сбор, обработку и далее по тексту',
        style: TextStyle(
          fontSize: 14,
        ),
      ),
      'checked': false,
    }, {
      'text': RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: 'Настоящим заявляю о присоедеинении к действующим редакциям ',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: 'Договора возмездногооказания информационных услуг',
              style: TextStyle(
                decoration: TextDecoration.underline,
                color: Color(0xffF3B63B),
              ),
            ),
            TextSpan(
              text: ' и далее по тексту',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
      'checked': false,
    },
  ];

  Widget _buildList(BuildContext context) {
    return Column(
      children: _items.map((Map item) {
        return CircleCheckbox(
          reverse: true,
          value: item['checked'],
          label: item['text'],
          onChange: (bool state) {
            setState(() {
              item['checked'] = state;
            });
          },
        );
      }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    UniqueKey key = new UniqueKey();
    return Layout(
      title: 'Подтверждение',
      child: Container(
        child: Builder(builder: _buildList),
      ),
      bottomPanel: PrimaryButton(
        key: key,
        enabled: _valid,
        text: 'Продолжить',
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(
            context,
            '/main',
            ModalRoute.withName('/main'),
          );
        },
      ),
    );
  }
}
