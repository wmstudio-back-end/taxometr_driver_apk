import 'package:flutter/material.dart';
import 'package:need_car/widgets/buttons/button_primary.dart';
import 'package:need_car/widgets/forms/form_text_field.dart';
import 'package:need_car/widgets/forms/select_page.dart';
import 'package:need_car/widgets/layout/layout.dart';
import 'package:need_car/widgets/pages/auth/auth_accept.dart';
import 'package:need_car/widgets/pages/common/pick_photo.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import 'package:need_car/widgets/layout/container_photo.dart';

class RegisterFormPage extends StatelessWidget {
  var formKey = GlobalKey<FormState>();

  final List<String> _inputs = [
    'Фамилия',
    'Имя',
    'Отчество',
    'Номер мобильного телефона',
    'E-mail',
  ];

  final List<Map> _photos = [
    {
      'screen-title': 'Паспортные данные',
      'photos': [
        {
          'title': 'Сфотографируйте паспорт',
          'description': 'Основной разворот паспорта с фото',
        },
      ],
    }, {
      'screen-title': 'Водительское удостоверение',
      'photos': [
        {
          'title': 'Сфотографируйте водительское удостоверение',
          'description': 'Лицевая сторона',
        }, {
          'title': 'Сфотографируйте водительское удостоверение',
          'description': 'Обратная сторона',
        },
      ],
    }, {
      'screen-title': 'Свидетельство о регистрации ТС',
      'photos': [
        {
          'title': 'Сфотографируйте Свидетельство о регистрации ТС',
          'description': 'Лицевая сторона',
        }, {
          'title': 'Сфотографируйте Свидетельство о регистрации ТС',
          'description': 'Обратная сторона',
        },
      ],
    },
    {
      'screen-title': 'Фотоконтроль Авто',
      'photos': [
        {
          'title': 'Сфотографируйте Автомобиль',
          'description': 'Спереди',
        }, {
          'title': 'Сфотографируйте Автомобиль',
          'description': 'Справа',
        }, {
          'title': 'Сфотографируйте Автомобиль',
          'description': 'Сзади',
        }, {
          'title': 'Сфотографируйте Автомобиль',
          'description': 'Слева',
        }, {
          'title': 'Сфотографируйте Автомобиль',
          'description': 'Регистрационный номер',
        },
      ],
    },
  ];

  final List<String> _cities = [
    'Воткинск',
    'Казань',
    'Ижевск',
    'Омск',
    'Москва',
    'Санкт-Петербург',
    'Екатеринбург',
    'Нижний Новгород',
    'Адлер',
    'Анапа',
    'Белорецк',
    'Березники',
  ];

  bool _autoValidate = false;
  var textEditingController = TextEditingController();
  var maskTextInputFormatter = MaskTextInputFormatter(mask: "+# (###) ###-##-##", filter: { "#": RegExp(r'[0-9]') });

  Widget _buildList(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: _inputs.map((input) {
          if(input[0] == 'Н'){
            return NCTextField(
              label: input,
              controller: textEditingController,
              inputFormatters: [maskTextInputFormatter],
              autocorrect: false,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(hintText: "+1 (123) 123-45-67",
                  fillColor: Colors.white,
                  filled: true,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10,
                  ),
                  labelStyle: Theme.of(context).textTheme.body2.copyWith(
                    color: Color(0xFFDBDBDB),
                  ),
                  labelText: 'Номер мобильного телефона'
              ),
              autoValidate: _autoValidate,
              validator: (value) {
                if (value.length < 18){
                  return 'Номер должен иметь 11 цифр';
                }
                return null;
              },
            );
          }
          return NCTextField(
            label: input,
            autoValidate: _autoValidate,
            validator: (value) {
              Pattern patternFIO = r'^[a-zA-Zа-яА-Я]+$';
              RegExp regexFIO = RegExp(patternFIO);
              Pattern patternEmail = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
              RegExp regexEmail = RegExp(patternEmail);
              if ((input[0] == 'Ф')  && (!regexFIO.hasMatch(value)|| value.length <2)) {
                return 'Фамилия должна содержать нe мeнee двух букв';
              }
              if (input[0] == 'И' && (!regexFIO.hasMatch(value)|| value.length <2)){
                return 'Имя должно содержать нe мeнee двух букв';
              }
              if (input[0] == 'О' && (!regexFIO.hasMatch(value)|| value.length <2)){
                return 'Отчество должно содержать нe мeнee двух букв';
              }
              if (input[0] == 'E'&& !regexEmail.hasMatch(value)){
                return 'Введите правильный Email';
              }
              return null;
//              if (value.isEmpty) {
//                return 'Введите данные';
//              }
//              return null;
            },
          );
        },).toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Layout(
      title: 'Личные данные',
      child: SingleChildScrollView(child: Builder(builder: _buildList)),
      bottomPanel: PrimaryButton(
        text: 'Продолжить',
        onPressed: () {
          // if (formKey.currentState.validate()) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => SelectPage(
                  onAccept: (List<String> result) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) =>
                          PickPhotoPage(
                            onFinished: () {
                              Navigator.push(
                                context, 
                                MaterialPageRoute(builder: (_) => AcceptPage()),
                              );
                            },
                            photos: _photos,
                          ),
                        )
                    );
                  },
                  title: 'Выбор города',
                  items: _cities,
                  singleItemMode: true,
                  withSearchBar: true,
                  searchBarLabel: 'Введите название города:',
                )
              ),
            );
          // } else {
          //   _autoValidate = true;
          // }
        },
      ),
    );
  }
}
