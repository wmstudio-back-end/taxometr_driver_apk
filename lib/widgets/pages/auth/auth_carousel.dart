import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:need_car/widgets/buttons/button_primary.dart';
import 'package:need_car/widgets/buttons/button_secondary.dart';
import 'package:need_car/widgets/pages/auth/auth_login_sentence.dart';

class AuthCarousel extends StatefulWidget {

  @override
  _AuthCarouselState createState() => _AuthCarouselState();
}

class _AuthCarouselState extends State<AuthCarousel> {
  
  final List<Map> _slides = [
    {
      'image': 'assets/images/money.png',
      'text': 'Пополните баланс,\nчтобы начать работу',
    }, {
      'image': 'assets/images/camera.png',
      'text': 'Пройдите\nфотоконтроль',
    }, {
      'image': 'assets/images/offers.png',
      'text': 'Принимайте\nзаказы',
    }
  ];

  final Duration _duration = new Duration(milliseconds: 500);
  final Curve _curve = Curves.easeInOut;

  int _currentPage = 0;

  void _skip(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (_) => LoginSentence(),
      ), 
    );
  }

  @override
  Widget build(BuildContext context) {

    CarouselSlider _slider = new CarouselSlider(
      enableInfiniteScroll: false,
      onPageChanged: (int index) {
        setState(() {
          _currentPage = index;
        });
      },
      items: _slides.map((slide) {
        return Container(
          child: Column(
            children: <Widget>[
              Image.asset(
                slide['image'],
                height: 140,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                '${slide['text']}',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline,
              ),
            ],
          ),
        );
      }).toList(),  
    );

    return Scaffold(
      body: FractionallySizedBox(
        heightFactor: 1,
        child: Stack(
          children: <Widget>[
            FractionallySizedBox(
              heightFactor: 1,
              child: _slider,
            ),
            Positioned(
              bottom: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    PrimaryButton(
                      text: 'Дальше',
                      onPressed: () {
                        if (_currentPage < _slides.length - 1) {
                          _slider.nextPage(
                            duration: _duration,
                            curve: _curve,
                          );
                        } else {
                          _skip(context);
                        }
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: List.generate(_slides.length, (int index) {
                            return GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                _slider.animateToPage(
                                  index,
                                  duration: _duration,
                                  curve: _curve,
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: _currentPage != index 
                                      ? Color(0xffF4F4F6)
                                      : Color(0xffFFDB4D),
                                  ),
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                        SecondaryButton(
                          text: 'Пропустить',
                          onPressed: () {
                            _skip(context);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}