import 'package:flutter/material.dart';
import 'package:need_car/widgets/buttons/button_primary.dart';
import 'package:need_car/widgets/layout/container.dart';
import 'package:need_car/widgets/layout/container_photo.dart';
import 'package:need_car/widgets/layout/layout.dart';

class PickPhotoPage extends StatefulWidget {

  final List<Map> photos;
  final int index;
  final Function onFinished;

  PickPhotoPage({
    @required this.photos,
    @required this.onFinished,
    this.index = 0,
  });

  @override
  _PickPhotoPageState createState() => _PickPhotoPageState();
}

class _PickPhotoPageState extends State<PickPhotoPage> {
  
  bool _valid = false;
  int _generation = 0;
  List<PhotoContainerState> containers = <PhotoContainerState>[];

  void forceRebuild() {
    setState(() {
      ++_generation;
    });
  }

  Widget _buildList(BuildContext context) {
    List<Map> photos = widget.photos[widget.index]['photos'];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: photos.map((photo) {
        return Container(
          margin: EdgeInsets.only(bottom: 20),
          child: PhotoContainer(
            title: photo['title'],
            description: photo['description'],
          ),
        );
      }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return PhotoValidator(
      generation: _generation,
      state: this,
      child: Layout(
        title: widget.photos[widget.index]['screen-title'],
        child: Stack(
          children: <Widget>[
            Scrollbar(
              child: SingleChildScrollView(
                child: PaddingContainer(child:
                Builder(builder: _buildList)
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: PrimaryButton(
                  key: new UniqueKey(),
                  enabled: true,
                  text: 'Продолжить',
                  onPressed: () {
                    if (widget.index < (widget.photos.length - 1)) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => PickPhotoPage(
                            index: widget.index + 1,
                            photos: widget.photos,
                            onFinished: widget.onFinished,
                          ),
                        ),
                      );
                    } else {
                      widget.onFinished();
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class PhotoValidator extends InheritedWidget {

  final int generation;
  // final bool valid = false;
  final _PickPhotoPageState state;

  PhotoValidator({
    this.generation,
    // this.valid = false,
    this.state,
    child
  }) : super(child: child) {
    // print('Состояние: ${state != null}');
    print('Поколение: $generation');
  }

  bool validate() {
    bool _valid = true;
    if (state.containers.length != 0) {
      for (PhotoContainerState container in state.containers) {
        if (!container.validate()) {
          _valid = false;
          break;
        }
      }
    } else {
      _valid = false;
    }
    print('Результат валидации: $_valid');
    return _valid;
  }

  void register(PhotoContainerState container) 
    => this.state.containers.add(container);

  @override
  bool updateShouldNotify(PhotoValidator oldWidget) 
    => oldWidget.generation != generation;

  static of(BuildContext context)
    => context.inheritFromWidgetOfExactType(PhotoValidator);
}