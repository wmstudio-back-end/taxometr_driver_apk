package com.unact.yandexmapkitexample;

import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;
import com.yandex.mapkit.MapKitFactory;

public class MainActivity extends FlutterActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    MapKitFactory.setApiKey("92e27e0c-cb00-45dd-9a91-800ccca2d5c2");
    GeneratedPluginRegistrant.registerWith(this);
  }
}
