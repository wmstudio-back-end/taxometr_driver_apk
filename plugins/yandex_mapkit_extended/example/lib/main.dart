import 'dart:async';

import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  Point _userLocation = new Point(
    latitude: 57.0486500,
    longitude: 53.9871700,
  );

  final List<Point> _points = [
    new Point(
      latitude: 53.720976,
      longitude: 91.44242300000001,
    ),
    new Point(
      latitude: 64.539304,
      longitude: 40.518735,
    ),
    new Point(
      latitude: 71.430564,
      longitude: 51.128422,
    ),
    new Point(
      latitude: 46.347869,
      longitude: 48.033574,
    ),
  ];

  static Point _point = Point(latitude: 59.945933, longitude: 30.320045);
  YandexMapController _yandexMapController;

  Future<void> _placemarks() async {
    _points.forEach((Point point) {
        _yandexMapController.addPlacemark(
        new Placemark(
          opacity: 1,
          point: point,
          iconName: 'lib/assets/place.png',
          onTap: (double latitude, double longitude) {
            List<Point> points = new List<Point>();
            points.add(_userLocation);
            points.add(point);
            _yandexMapController.buildRoute(points);
          }
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('YandexMapkit Plugin')
        ),
        body: Column(
          children: [
            Expanded(
              child: YandexMap(
                onMapCreated: (controller) async {
                  _yandexMapController = controller;
                  await _placemarks();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
