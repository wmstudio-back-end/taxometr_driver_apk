package com.unact.yandexmapkit;

import java.util.List;
import java.util.ArrayList;

import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.directions.driving.DrivingSession;
import com.yandex.mapkit.directions.driving.DrivingRoute;

abstract class YandexMapkitRouter implements DrivingSession.DrivingRouteListener {

    @Override
    public void onDrivingRoutes(List<DrivingRoute> routes) {}
}