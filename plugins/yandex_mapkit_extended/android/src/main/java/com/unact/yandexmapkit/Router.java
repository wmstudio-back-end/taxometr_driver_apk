package com.unact.yandexmapkit;

import java.util.List;
import java.util.ArrayList;

import com.yandex.mapkit.directions.driving.DrivingRoute;
import com.yandex.runtime.Error;

public class Router extends YandexMapkitRouter {

    @Override
    public void onDrivingRoutes(List<DrivingRoute> routes) {
        super.onDrivingRoutes(routes);
    }

    @Override
    public void onDrivingRoutesError(Error e) {

    }
}