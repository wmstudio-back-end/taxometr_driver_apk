package com.unact.yandexmapkit;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.view.View;

import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.BoundingBox;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.layers.ObjectEvent;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.mapkit.user_location.UserLocationLayer;
import com.yandex.mapkit.user_location.UserLocationObjectListener;
import com.yandex.mapkit.user_location.UserLocationView;
import com.yandex.runtime.Error;
import com.yandex.runtime.image.ImageProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.platform.PlatformView;

import com.yandex.mapkit.directions.driving.DrivingSession;
import com.yandex.mapkit.directions.driving.DrivingOptions;
import com.yandex.mapkit.directions.driving.DrivingRouter;
import com.yandex.mapkit.directions.driving.DrivingRoute;
import com.yandex.mapkit.directions.DirectionsFactory;

import com.yandex.mapkit.RequestPoint;
import com.yandex.mapkit.RequestPointType;
import com.yandex.mapkit.geometry.Point;

import com.yandex.mapkit.user_location.UserLocationObjectListener;

class YandexMapController implements PlatformView, MethodChannel.MethodCallHandler, DrivingSession.DrivingRouteListener {
  private final MapView mapView;
  private final MethodChannel methodChannel;
  private final PluginRegistry.Registrar pluginRegistrar;
  private YandexUserLocationObjectListener yandexUserLocationObjectListener;
  private YandexMapObjectTapListener yandexMapObjectTapListener;
  private List<PlacemarkMapObject> placemarks = new ArrayList<>();
  private String userLocationIconName;

  private MapObjectCollection mapObjects;
  private final Point ROUTE_START_LOCATION = new Point(59.959194, 30.407094);
  private final Point ROUTE_END_LOCATION = new Point(55.733330, 37.587649);
  private DrivingRouter drivingRouter;
  private DrivingSession drivingSession;

  private UserLocationLayer userLocationLayer;

  public YandexMapController(int id, Context context, PluginRegistry.Registrar registrar) {
    MapKitFactory.initialize(context);
    DirectionsFactory.initialize(context);
    mapView = new MapView(context);
    MapKitFactory.getInstance().onStart();
    mapView.onStart();
    
    mapObjects = mapView.getMap().getMapObjects().addCollection();
    drivingRouter = DirectionsFactory.getInstance().createDrivingRouter();

    pluginRegistrar = registrar;
    yandexMapObjectTapListener = new YandexMapObjectTapListener();

    yandexUserLocationObjectListener = new YandexUserLocationObjectListener(registrar);

    userLocationLayer = mapView.getMap().getUserLocationLayer();
    userLocationLayer.setEnabled(true);
    userLocationLayer.setHeadingEnabled(true);
    userLocationLayer.setObjectListener(yandexUserLocationObjectListener);

    methodChannel = new MethodChannel(registrar.messenger(), "yandex_mapkit/yandex_map_" + id);
    methodChannel.setMethodCallHandler(this);
  }

  public void buildRoute(MethodCall call) {
    List<Map<String, Object>> routes = (List<Map<String, Object>>)call.arguments;
    List<RequestPoint> wayPoints = new ArrayList<RequestPoint>();
    for (Map<String, Object> route : routes) {
        Point point = new Point((double)route.get("latitude"), (double)route.get("longitude"));
        wayPoints.add(new RequestPoint(
            point,
            RequestPointType.WAYPOINT,
            null
        ));
    }
    DrivingOptions options = new DrivingOptions();
    options.setAlternativeCount(1);
    drivingSession = drivingRouter.requestRoutes(wayPoints, options, this);
  }

  @Override
  public void onDrivingRoutes(@NonNull List<DrivingRoute> routes) {
    mapObjects.clear();
    for (DrivingRoute route : routes) {
      mapObjects.addPolyline(route.getGeometry()).setOutlineColor(2);
      mapObjects.addPolyline(route.getGeometry());
    }
  }

  @Override
  public void onDrivingRoutesError(Error e) {

  }

  @Override
  public View getView() {
    return mapView;
  }

  @Override
  public void dispose() {
    mapView.onStop();
    MapKitFactory.getInstance().onStop();
  }

  @SuppressWarnings("unchecked")
  private void showUserLayer(MethodCall call) {
    if (!hasLocationPermission()) return;

    Map<String, Object> params = ((Map<String, Object>) call.arguments);
    userLocationIconName = (String) params.get("iconName");

    UserLocationLayer userLocationLayer = mapView.getMap().getUserLocationLayer();
    userLocationLayer.setEnabled(true);
    userLocationLayer.setHeadingEnabled(true);
    userLocationLayer.setObjectListener(yandexUserLocationObjectListener);
  }

  private void hideUserLayer() {
    if (!hasLocationPermission()) return;

    UserLocationLayer userLocationLayer = mapView.getMap().getUserLocationLayer();
    userLocationLayer.setEnabled(false);
  }

  @SuppressWarnings("unchecked")
  private void move(MethodCall call) {
    Map<String, Object> params = ((Map<String, Object>) call.arguments);
    Point point = new Point(((Double) params.get("latitude")), ((Double) params.get("longitude")));
    CameraPosition cameraPosition = new CameraPosition(
        point,
        ((Double) params.get("zoom")).floatValue(),
        ((Double) params.get("azimuth")).floatValue(),
        ((Double) params.get("tilt")).floatValue()
    );

    moveWithParams(params, cameraPosition);
  }

  @SuppressWarnings("unchecked")
  private void setBounds(MethodCall call) {
    Map<String, Object> params = ((Map<String, Object>) call.arguments);
    BoundingBox boundingBox = new BoundingBox(
        new Point(((Double) params.get("southWestLatitude")), ((Double) params.get("southWestLongitude"))),
        new Point(((Double) params.get("northEastLatitude")), ((Double) params.get("northEastLongitude")))
    );

    moveWithParams(params, mapView.getMap().cameraPosition(boundingBox));
  }

  @SuppressWarnings("unchecked")
  private void addPlacemark(MethodCall call) {
    addPlacemarkToMap(((Map<String, Object>) call.arguments));
  }

  private void movePlacemarkTo(MethodCall call) {

  }

  @SuppressWarnings("unchecked")
  private void removePlacemark(MethodCall call) {
    Map<String, Object> params = ((Map<String, Object>) call.arguments);
    MapObjectCollection mapObjects = mapView.getMap().getMapObjects();
    Iterator<PlacemarkMapObject> iterator = placemarks.iterator();

    while (iterator.hasNext()) {
      PlacemarkMapObject placemarkMapObject = iterator.next();
      if (placemarkMapObject.getUserData().equals(params.get("hashCode"))) {
        mapObjects.remove(placemarkMapObject);
        iterator.remove();
      }
    }
  }

  private void addPlacemarkToMap(@NonNull Map<String, Object> params) {
    Point point = new Point(
      ((Double) params.get("latitude")),
      ((Double) params.get("longitude"))
    );
    MapObjectCollection mapObjects = mapView.getMap().getMapObjects();
    PlacemarkMapObject placemark = mapObjects.addPlacemark(point);
    String iconName = (String) params.get("iconName");

    placemark.setUserData(params.get("hashCode"));
    placemark.setOpacity(((Double) params.get("opacity")).floatValue());
    placemark.setDraggable((Boolean) params.get("isDraggable"));
    placemark.addTapListener(yandexMapObjectTapListener);

    if (iconName != null) {
      placemark.setIcon(ImageProvider.fromAsset(mapView.getContext(), pluginRegistrar.lookupKeyForAsset(iconName)));
    }

    placemarks.add(placemark);
  }

  private void moveWithParams(Map<String, Object> params, CameraPosition cameraPosition) {
    if (((Boolean) params.get("animate"))) {
      Animation.Type type = ((Boolean) params.get("smoothAnimation")) ? Animation.Type.SMOOTH : Animation.Type.LINEAR;
      Animation animation = new Animation(type, ((Double) params.get("animationDuration")).floatValue());

      mapView.getMap().move(cameraPosition, animation, null);
    } else {
      mapView.getMap().move(cameraPosition);
    }
  }

  private boolean hasLocationPermission() {
    int permissionState = ActivityCompat.checkSelfPermission(mapView.getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
    return permissionState == PackageManager.PERMISSION_GRANTED;
  }

  private void zoomIn() {
    zoom(1f);
  }

  private void zoomOut() {
    zoom(-1f);
  }

  private void zoom(float step) {
    Point zoomPoint = mapView.getMap().getCameraPosition().getTarget();
    float currentZoom = mapView.getMap().getCameraPosition().getZoom();
    float tilt = mapView.getMap().getCameraPosition().getTilt();
    float azimuth = mapView.getMap().getCameraPosition().getAzimuth();
    mapView.getMap().move(
            new CameraPosition(
                zoomPoint,
                currentZoom+step,
                tilt,
                azimuth
            ),
            new Animation(Animation.Type.SMOOTH, 1),
            null);
  }

  @Override
  public void onMethodCall(MethodCall call, MethodChannel.Result result) {
    switch (call.method) {
      case "showUserLayer":
        showUserLayer(call);
        result.success(null);
        break;
      case "hideUserLayer":
        hideUserLayer();
        result.success(null);
        break;
      case "move":
        move(call);
        result.success(null);
        break;
      case "setBounds":
        setBounds(call);
        result.success(null);
        break;
      case "addPlacemark":
        addPlacemark(call);
        result.success(null);
        break;
      case "removePlacemark":
        removePlacemark(call);
        result.success(null);
        break;
      case "zoomIn":
        zoomIn();
        result.success(null);
        break;
      case "zoomOut":
        zoomOut();
        result.success(null);
        break;
      case "buildRoute":
        buildRoute(call);
        result.success(null);
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  private class YandexUserLocationObjectListener implements UserLocationObjectListener {
    private PluginRegistry.Registrar pluginRegistrar;

    private YandexUserLocationObjectListener(PluginRegistry.Registrar pluginRegistrar) {
      this.pluginRegistrar = pluginRegistrar;
    }

    public void onObjectAdded(UserLocationView view) {
      System.out.println(view);
      view.getPin().setIcon(
          ImageProvider.fromAsset(
              pluginRegistrar.activity(),
              pluginRegistrar.lookupKeyForAsset(userLocationIconName)
          )
      );
    }

    public void onObjectRemoved(UserLocationView view) {}

    public void onObjectUpdated(UserLocationView view, ObjectEvent event) {
      System.out.println(event);
    }
  }

  private class YandexMapObjectTapListener implements MapObjectTapListener {
    public boolean onMapObjectTap(MapObject mapObject, Point point) {
      Map<String, Object> arguments = new HashMap<>();
      arguments.put("hashCode", mapObject.getUserData());
      arguments.put("latitude", point.getLatitude());
      arguments.put("longitude", point.getLongitude());

      if (drivingSession != null) {
        drivingSession.cancel();
      }

      methodChannel.invokeMethod("onMapObjectTap", arguments);

      return true;
    }
  }
}
